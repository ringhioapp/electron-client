'use strict';

const client = require('socket.io-client'),
    notifier = require('node-notifier'),
    path = require('path');

const config = require('./config.json');

const sclient = () => {

  var tray = null;

  var socket = null;
  var ringing = false;

  var connect = function(data) {
    console.log('connect', data);

    socket = client(config.url)


    socket.on('connect', function() {
        socket.emit('waitfor', {
            phone: data.phone,
            pin: data.pin
        });
    });

    socket.on('nofity', function(data) {
        console.log('notified', data);
        var caller = (data.caller_name) ? data.caller_name : (data.caller) ? data.caller : 'Unkown';
        if (data.status == 'RINGING') {
          ringing = true;
            blinkTrayIcon(0);
            notifier.notify({
                title: 'Ringhio',
                message: caller + ' is calling',
                sound: true, // Only Notification Center or Windows Toasters
                wait: false // Wait with callback, until user action is taken against notification
            }, function(err, response) {
                // Response is response from notification
            });

            notifier.on('click', function(notifierObject, options) {
                // Triggers if `wait: true` and user clicks notification
            });

            notifier.on('timeout', function(notifierObject, options) {
                // Triggers if `wait: true` and notification closes

            });
        } else {
          ringing = false;
        }
    });

  }

  var blinkTrayIcon = function(c) {
    if(c > 100 || !ringing) {
      tray.setImage(__dirname + '/favicon.png');
      return;
    }
    setTimeout(function() {
      var image = (c % 2) ? '/favicon.png' : '/favicon2.png';
      tray.setImage(__dirname + image);
      blinkTrayIcon(c+1);
    }, 500)
  }

  var disconnect = function() {
    ringing = false;
    console.log('disconnect');
  }



  return {
    connect: connect,
    disconnect: disconnect,
    setTray: function(_tray) {
      tray = _tray;
    }
  }
}

module.exports = sclient()
