const {
    app,
    Menu,
    Tray,
    BrowserWindow
} = require('electron')

const client = require('socket.io-client'),
    notifier = require('node-notifier'),
    path = require('path');

const sclient = require('./sclient.js');

const ElectronSettings = require('electron-settings');

let settings = new ElectronSettings();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 400,
        height: 400,
        resizable: false
    })

    // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/index.html`)

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}

var phone = settings.get('phone');

let notificationOn = typeof(phone) == 'string';

let tray = null

app.on('ready', () => {
    tray = new Tray(__dirname + '/favicon.png')
    const contextMenu = Menu.buildFromTemplate([{
        label: 'Notification',
        type: 'checkbox',
        checked: notificationOn,
        click(item, focusedWindow) {
            notificationOn = !notificationOn;
            //item.checked = notificationOn;
            if(notificationOn) {
              sclient.connect();
            } else {
              sclient.disconnect();
            }
        }
    }, {
        type: 'separator'
    }, {
        label: 'Quit',
        type: 'checkbox',
        click(item, focusedWindow) {
            app.quit()
        }
    }]);
    tray.setToolTip('Ringhio')
    tray.setContextMenu(contextMenu);

    sclient.setTray(tray);

    if(!notificationOn) {
      createWindow();
    } else {
      mainWindow = null
      var data = {
        phone: settings.get('phone'),
        pin: settings.get('pin'),
      }
      sclient.connect(data);
    }
})

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q



})

app.on('activate', function() {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
