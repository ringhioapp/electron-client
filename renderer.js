// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const remote = require('electron').remote;

const sclient = require ('./sclient.js');
const phoneEl = document.querySelector('.phone-input');
const pinEl = document.querySelector('.pin-input');
const connectEl = document.querySelector('.connect-button');

const ElectronSettings = require('electron-settings');

let settings = new ElectronSettings();

var phone = settings.get('phone')
if(phone)
  phoneEl.value = phone;
var pin = settings.get('pin');
if(pin)
  pinEl.value = pin;

connectEl.addEventListener('click', function () {
  var phone = phoneEl.value;
  var pin = pinEl.value;
  if(phone == '' || pin == '') {
    alert('Please fill phone and pin values');
    return;
  }

  settings.set('phone',phone);
  settings.set('pin',pin);

  var data = {phone: phone, pin: pin};

  sclient.connect(data);

  var window = remote.getCurrentWindow();
  window.close();
  
});
